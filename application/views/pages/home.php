<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta lang="de">
		<title>new develapper</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="<?= base_url() ?>css/style.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	</head>
	<body>
		<header>
			<h2>
				<a id="menu_btn" href="#navbar"><i class="fa fa-bars"></i></a>
				<a id="logo" href="#"><i class="fa fa-magic fa-flip-horizontal"></i></a>
			</h2>
			<h2 id="apptitle">DevelApper</h2>
			<nav id="user_menu">
			<ul>
				<li class="dropdown">
					<a class="dropbtn" href="#">login</a>
				</li>
			</ul>
				
			</nav>
		</header>
		<nav id="proj_menu" class="hide">
			<ul>
				<li><a href="#">first</a>
					<ul>
						<li><a href="#">first 1</a></li>
						<li><a href="#">first 2</a></li>
						<li><a href="#">first 3</a></li>
						<li><a href="#">first 4</a></li>
					</ul>
				</li>
				<li><a href="#">second</a>
					<ul>
						<li><a href="#">second 1</a></li>
						<li><a href="#">second 2</a></li>
						<li><a href="#">second 3</a></li>
						<li><a href="#">second 4</a></li>
					</ul>
				</li>
				<li><a href="#">third</a></li>
				<li><a href="#">forth</a>
					<ul>
						<li><a href="#">forth 1</a></li>
						<li><a href="#">forth 2</a></li>
						<li><a href="#">forth 3</a></li>
						<li><a href="#">forth 4</a></li>
					</ul>
				</li>
			</ul>
		</nav>
		<section>
			<article>
				<h2>Develapper</h2>
				<p>Develapper wurde entwickelt, um Nichtexperten ein Werkzeug zur Verfügung zu stellen eine Menüstruktur zu entwickeln.</p>
				<p>Mit diesem Programm können Sie Ihre Bedürfnisse ganz ohne Programmierkenntnisse auf den Bildschirm zaubern, ohne ein aufwändiges Pflichtenheft zu schreiben. Jeder Programmierer wird sofort erkennen was Sie benötigen und Ihnen die Applikation so programmieren wie Sie sich Ihr neues Werkzeug (Programm) vorgestellt haben.</p>
			</article>
			<aside>
				<article>
					<h2>Super Sache...</h2>
					<p>Ich verwende das System regelmässig! Bin begeistert.</p><hr>
					<p>superuser</p>
				</article>
				<article>
					<h2>Totaler Mist</h2>
					<p>Unbrauchbar! Viele Fehler, funktioniert nicht...</p><hr>
					<p>miesepeter</p>
				</article>
			</aside>
		</section>
		<footer>
			<p>© <?php echo date('Y'); ?><span><a href="<?php echo base_url() ?>contact">Philippe Berberat<i class="fa fa-envelope-o"></i></a></span>
			</p>
			<p class="devinfo">Page rendered in {elapsed_time} seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version'.' '.CI_VERSION : '' ?>
			</p>
		</footer>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script src="js/app.js"></script>
	</body>
</html>
